import java.util.ArrayList;
import java.util.List;

public class Phonebook implements PhonebookLike {
    private final List<Contact> contacts = new ArrayList<>();

    @Override
    public Contact[] getContacts() {
        Contact[] contactsForCheck = contacts.toArray(new Contact[contacts.size()]);
        return contactsForCheck;
    }

    @Override
    public Contact[] getContacts(String name) {
        List<Contact> tempList = new ArrayList<>();

        for (Contact c : contacts) {
            if (c.getName().equals(name)) {
                tempList.add(c);
            }
        }

        Contact[] contactsForCheck = tempList.toArray(new Contact[tempList.size()]);
        return contactsForCheck;
    }

    @Override
    public boolean contains(String name) {
        for (Contact c : contacts) {
            if (c.getName().equals(name)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Phonebook add(Contact contact) {
        contacts.add(contact);
        return this;
    }

    @Override
    public Phonebook remove(Contact contact) {
        contacts.remove(contact);
        return this;
    }

    @Override
    public PhonebookLike remove(String name) {
        List<Contact> contactsToRemove = new ArrayList<>();

        for (Contact c : contacts) {
            if (c.getName().equals(name)) {
                contactsToRemove.add(c);
            }
        }

        for (Contact c : contactsToRemove) {
            contacts.remove(c);
        }
        return this;
    }
}
