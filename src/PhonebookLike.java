public interface PhonebookLike {
    // @.pre true
    // @.post RESULT == (all contacts)
    Contact[] getContacts();
    // @.pre true
    // @.post RESULT == getContacts().length
    default int size() { return getContacts().length; }
    // @.pre name != null
    // @.post FORALL(c: RESULT; contains(c) && c.name().equals(name))
    Contact[] getContacts(String name);
    // @.pre name != null
    // @.post RESULT == getContacts(name).length
    default int matchingContacts(String name) { return getContacts(name).length; }
    // @.pre name != null
    // @.post RESULT == EXISTS(c: getContacts(); c.name().equals(name))
    boolean contains(String name);

    // @.pre contact != null
    // @.post contains(contact) && RESULT.remove(contact).equals(OLD(this))
    PhonebookLike add(Contact contact);
    // @.pre contact != null && contains(contact)
    // @.post !contains(contact) && RESULT.add(contact).equals(OLD(this))
    PhonebookLike remove(Contact contact);
    // @.pre name != null && contains(name)
    // @.post !contains(name) && RESULT.add(OLD(this).getContacts(name)).equals(OLD(this))
    PhonebookLike remove(String name);
    // @.classInvariant size() >= 0
}
