public class Main {
    public static void main(String[] args) {
        Contact contact1 = new Contact("Vsevolod", 549219);
        Contact contact2 = new Contact("Diana", 549709);
        Contact contact3 = new Contact("Mama", 549219);


        Phonebook phonebook = new Phonebook();

        phonebook.add(contact1);
        phonebook.add(contact2);
        phonebook.add(contact3);

        Contact[] allContacts = phonebook.getContacts();
        System.out.println("All contacts");
        for (Contact c : allContacts) {
            System.out.println(c.getName() + ": " + c.getNumber());
        }

        String nameToRemove = "Vsevolod";

        phonebook.remove(nameToRemove);

        allContacts = phonebook.getContacts();
        System.out.println("All contacts after remove");
        for (Contact c : allContacts) {
            System.out.println(c.getName() + ": " + c.getNumber());
        }
    }
}